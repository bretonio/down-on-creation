import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import { ButtonToolbar } from 'react-bootstrap';

function App() {
  const [date, setDate] = useState(null);
  useEffect(() => {
    async function getDate() {
      const res = await fetch('/api/date');
      const newDate = await res.text();
      setDate(newDate);
    }
    getDate();
  }, []);
  return (
    <main>
		<h1><i class="fas fa-chess fa-4x"></i></h1>
      <h1>Reactionary Coming Attraction</h1>
      <h2>
        Deployed with{' '}
        <a
          href="https://zeit.co/docs"
          target="_blank"
          rel="noreferrer noopener"
        >
          Necessary Oratory Words
        </a>
      </h2>

			<ButtonToolbar>
			  <Button variant="primary">Primary</Button>
			  <Button variant="secondary">Secondary</Button>
			  <Button variant="success">Success</Button>
			  <Button variant="warning">Warning</Button>
			  <Button variant="danger">Danger</Button>
			  <Button variant="info">Info</Button>
			  <Button variant="light">Light</Button>
			  <Button variant="dark">Dark</Button>

			</ButtonToolbar>

    </main>
  );
}

export default App;
